$(function () {
    // ========== Variables =============
    var markers;

    // ========== functions =============

    function templateCard(elm, items) {
        let i;
        let itemSel;
        let listCards = "";
        let firstCard = (obj) => `<div data-key="${obj.key}" id="header-card-cont">
                        <span class="title__card_sala__ventas"><b>${ obj.name}</b></span>
                        <img class="card" src="${ obj.img}" alt="${obj.name}">
                    </div>
                    <div class="" style="padding: 5%">
                        <div class="row">
                        <div class="col-1"></div>
                            <div class="col-1"><img src="../assets/images/24px.png" alt=""></div>
                            <div class="col-9">Dirección</div>
                            <div class="col-2"></div>
                            <div class="col-9">${ obj.address}</div>
                        </div>
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-1"><img src="../assets/images/24px.png" alt=""></div>
                            <div class="col-9 texto__numero">${ obj.phone}</div>
                            <div class="col-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-1"><img src="../assets/images/24px.png" alt=""></div>
                            <div class="col-9">${ obj.email}</div>
                            <div class="col-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-1"><img src="../assets/images/24px.png" alt=""></div>
                            <div class="col-9">${ obj.schedule}</div>
                            <div class="col-1"></div>
                        </div>
                    </div>`;
        let htmlList = (obj, idx) => `<div class="row card-item" data-key="${obj.key}" data-id="${ idx }">
                        <div class="col-2"><img src="${ obj.logo}" alt="${obj.name}"></div>
                        <div class="col-10 letter__card__gratamira">${ obj.name}<br>
                            <p class="letter__2">${ obj.address}</p>
                        </div>
                    </div>`;

        for (i = 0; i < items.length; i++) {
            if (typeof elm == 'object') {
                if (items[i].key != elm.key) {
                    listCards += htmlList(items[i], i);
                } else {
                    itemSel = items[i];
                }
            } else {
                if (items[i].key != elm) {
                    listCards += htmlList(items[i], i);
                } else {
                    itemSel = items[i];
                }
            }
        }

        console.log(itemSel)

        $("#first-card").html(firstCard(itemSel));
        if ( listCards.length > 1 ) {
            $("#list-cards").removeClass("d-none");
            $("#list-cards").html(listCards);
        } else {
            $("#list-cards").addClass("d-none");
        }
        $("#list-cards").niceScroll({
            cursorcolor: "#037F43",
            cursorborder: "1px solid #037F43"
        });

    }
    function clickItem(items) {
        $("#list-cards .card-item").on("click", function (e) {
            let key = $(e.currentTarget).attr("data-key");
            let idx = $(e.currentTarget).attr("data-id");
            console.log(key)

            let itemSel = items.filter((item, idx) => {
                return item.key == Number(key)
            });

            console.log(itemSel)

            templateCard(itemSel[0], items);
            // attachMessage(itemSel);
            google.maps.event.trigger(markers[idx], 'click');
            clickItem(items);
        });
    }
    function initMap(options, data) {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: options,
            zoom: 9
        });

        var contentHtml = function (obj) {
            return `<h4 data-key="${obj.key}" class="">${obj.title}</h4>`;
        }

        // var marker = new google.maps.Marker({
        //     position: options,
        //     title: 'Tu ubicacion'
        // })
        //marker.setMap(map);

        var infowindow = new google.maps.InfoWindow();

        markers = data.map((place) => {
            return new google.maps.Marker({
                position: {
                    lat: Number(place.lat),
                    lng: Number(place.lng)
                },
                map: map,
                title: place.name,
                key: place.key,
                icon: place.icon
                // img: place.img,
                // address: place.SedeDireccion,
                // phone: place.SedeTelefono
            })
        });

        console.log(markers)

        markers.forEach(item => {
            if (item) {
                let content = contentHtml(item);
                google.maps.event.addListener(item, 'click', (function (item, content, infowindow) {
                    return function () {
                        infowindow.setContent(content);
                        infowindow.open(map, item);
                        // windows.push(infowindow)
                        console.log(item.icon)
                        var itemsDom = [...document.querySelectorAll("#list-cards .card-item")];

                        templateCard(item.key, data);
                        clickItem(data);

                        google.maps.event.addListener(map, 'click', function () {
                            infowindow.close();
                        });
                    };
                })(item, content, infowindow));
            }
        });
    }
    function markersMap(resp) {
        let dataResp = resp;

        //navigator.geolocation.getCurrentPosition(function (data) {
            let currentPos = {
                lat: dataResp[0].lat,
                lng: dataResp[0].lng
            }
            initMap(currentPos, dataResp);
        //})
    }
    function renderCities(cities) {
        let i = 0;
        let options = '<option value="" disabled selected>Seleccionar</option>';
        let arrCities = [];

        for (i; i < cities.length; i++) {
            if (arrCities.indexOf(cities[i].city) == -1) {
                console.log("Entro")
                arrCities.push(cities[i].city);
                options += `<option value="${cities[i].city}" data-lat="${cities[i].lat}" data-lng="${cities[i].lng}">${cities[i].city}</option>`;
            }
        }
        $("#cities").html(options);
        console.log(arrCities)
    }

    function renderSectors(sectors) {
        let i = 0;
        let arrSectors = [];
        let options = '<option value="" disabled selected>Seleccionar</option>';

        for (i; i < sectors.length; i++) {
            if (arrSectors.indexOf(sectors[i].sector) == -1) {
                arrSectors.push(sectors[i].sector);
                options += `<option value="${sectors[i].sector}" data-lat="${sectors[i].lat}" data-lng="${sectors[i].lng}">${sectors[i].sector}</option>`;
            }
        }
        console.log()
        $("#sectors").html(options);
        $("#sectors option[value='Bogotá'").prop("selected",true);
    }

    function getPlaces() {
        $.getJSON("../js/json/headquarters.json", (resp) => {
            let places = resp;
            let sectors = places.filter(item => {
                return item.city == "Bogotá"
            });

            console.log(places)

            // Run functions
            markersMap(sectors);
            renderCities(places);
            renderSectors(sectors);
            templateCard(sectors[0], sectors);
            clickItem(sectors);

            // Events
            $('#cities').change(function () {

                let headquarters = places.filter(place => {
                    return place.city == this.value;
                });

                // $("#option-city").removeClass("d-none");
                renderSectors(headquarters);
                templateCard(headquarters[0], headquarters);
                markersMap(headquarters);
                clickItem(headquarters);
                //mapComponent(headquarters);
            });

            $('#sectors').change(function () {
                console.log(this.value)
                let headquarters = places.filter(place => {
                    return place.sector == this.value;
                });

                templateCard(headquarters[0], headquarters);
                markersMap(headquarters);
                clickItem(headquarters);
            });
        });
    }


    // ========== Run functions =============
    getPlaces();

    // ========== Events =============
})